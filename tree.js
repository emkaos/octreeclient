var tree_Octree;

function init_tree(){
    start_timer("tree");
    //tree_Octree = server_getTree();
    importScripts("http://127.0.0.1:8000/getTree");
    stop_timer("tree");
    print_results("tree");
}


function tree_getColorForNode(node) {
    var f = (node['size'][0] * node['size'][1] * node['size'][2]) / 10000000000;
    var g = node['point_count'] / f;
    var b = node['edge_count'] / f;
    return new SFColor(0, b, g);
}

function tree_getTransparencyForNode(node) {
    var f = (node['size'][0] * node['size'][1] * node['size'][2]) / 10000000000.5;
    var g = node['point_count'] / f;
    var b = node['edge_count'] / f;
    return new SFFloat(1 - b - g);
}

function localTreeNode(level, num) {
    var node = tree_Octree;
    var n = num;
    for (var i=0; i < level; i++) {
        node = node.branches[n % 10];
        n = ~~(n / 10);  // integer division
    }
    return node;
}
