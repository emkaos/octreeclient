
//testpath = [[0,0,20000],[10000,0,20000], [0, 0, 10000], [20000, 5000, 10000]];
//testpath = [[0,0,20000],[5000,0,15000], [0, 0, 10000], [-10000, 0, 5000]];
testpath = [[0,0,20000, 'node number 1' ],[5000,0,18000, "node2"], [0, 0, 17000, "node3"], [-10000, 0, 18000, "node4"]];


var motion_orientation;
var motion_target;
var motion_startpoint;

var motion_speed = SETTINGS_path_speed; // meters per second
var rotation_speed = SETTINGS_path_rotation_speed; // degree per second?

var motion_current_path;
var motion_current_index;

// to synchronize timer, sometimes unwanted isActive=false send
var is_rotating = false;
var is_moving = false;

// loop
var motion_loop = true;

// paused or start new
var paused = false;

function init_motion() {
    motion_timer = Browser.currentScene.getNamedNode("motion_timer");
    rotation_timer = Browser.currentScene.getNamedNode("rotation_timer");
    position_interpolator = Browser.currentScene.getNamedNode("position_interpolator");
    orientation_interpolator = Browser.currentScene.getNamedNode("orientation_interpolator");
}

function motion_startPath() {
    // set position to first point
    // set motion_target; 
    motion_current_index = 0;
    motion_target_index = 1;
    motion_current_path = testpath;
    motion_startpoint = new SFVec3f(testpath[0][0], testpath[0][1], testpath[0][2]);
    motion_target = new SFVec3f(testpath[1][0], testpath[1][1], testpath[1][2]);
    motion_moveToPoint();
}

function motion_moveToPoint() {
    // speak 
    if (SETTINGS_speak_path === true) {
        sound_speak(motion_current_path[motion_current_index][3]);
    }

    viewpoint.position = motion_startpoint;
    motion_setSpeed(motion_startpoint, motion_target);
   
    // set position interpolator values
    var kv = new MFVec3f(motion_startpoint);
    kv[kv.length] = motion_target;
    position_interpolator.keyValue = kv;

    // this starts rotating to orientation 
    motion_setOrientation(motion_startpoint, motion_target);

}

function motion_timerActive(value) {
    if (value === false && is_moving === true) {
        // target reached
        motion_timer.enabled = false;
        is_moving = false;
        print("Motion finished.");
        motion_nextTarget();
    } else {
        print("Motion start.");
    }
}

function motion_rotationActive(value) {
    if (value === false && is_rotating === true) {
        print("Rotation finished.");
        rotation_timer.enabled = false;
        orientation_interpolator.enabled = false;
        is_rotating = false;
        // orientation set, start moving
        var start_time = new Date().getTime() / 1000;
        motion_timer.set_startTime = start_time + 0;
        motion_timer.enabled = true;
        is_moving = true;
    } else {
        print("Rotation start.");
    }
}

function motion_nextTarget() {
    motion_current_index++;
    motion_target_index++;

    if (motion_target_index < motion_current_path.length) { 
        motion_startpoint = motion_target;
        var pathdata = motion_current_path[motion_target_index];
        motion_target =  new SFVec3f(pathdata[0], pathdata[1], pathdata[2]);
        motion_moveToPoint();
    } else {
        if (motion_loop === true) {
            motion_current_path.reverse();
            motion_current_index = 0;
            motion_target_index = 1;
            motion_startpoint = motion_target;
            pathdata = motion_current_path[motion_target_index];
            motion_target =  new SFVec3f(pathdata[0], pathdata[1], pathdata[2]);
            motion_moveToPoint();
        }
    }
}

function motion_setOrientation(from, to) {
    // calculate rotation
    var v1 = base_los.normalize();
    var v2 = to.subtract(from).normalize();
    var angle = Math.acos(v1.dot(v2));
    var axis = v1.cross(v2).normalize();
    var target_orientation = new SFRotation(axis.x, axis.y, axis.z, angle);

    // set up rotation interpolator
    motion_setRotationSpeed(angle);
    current_orientation = viewpoint.orientation;
    var kv = new MFRotation(current_orientation);
    kv[kv.length] =  target_orientation;
    orientation_interpolator.keyValue = kv;

    // start timer for rotation
    var start_time = new Date().getTime() / 1000;
    rotation_timer.set_startTime = start_time + 1;
    rotation_timer.enabled = true;
    is_rotating = true;
    orientation_interpolator.enabled = true;
}

function motion_setRotationSpeed(rads) {
    var deg = rads * ( 180 / 3.14159265 );
   // print(deg);
   // print(deg / rotation_speed);
    rotation_timer.cycleInterval = new SFFloat(deg / rotation_speed); 
}

function motion_setSpeed(from, to) {
    var dist = from.subtract(to).length(); 
    //print("Distance: " + dist);
    var iv = dist / motion_speed;
    motion_timer.cycleInterval = new SFFloat(iv);
}

function motion_movementPause() {
    motion_timer.pauseTime = new Date().getTime() / 1000;
}

function motion_movementResume() {
    //viewpoint.position = position_interpolator.value_changed;
    // reset orientation
   // motion_setOrientation(motion_startpoint, motion_target);
   //
    viewpoint.orientation = orientation_interpolator.value_changed;
    motion_timer.resumeTime = new Date().getTime() / 1000;
}

function motion_rotationPause() {
    rotation_timer.pauseTime = new Date().getTime() / 1000;
}

function motion_rotationResume() {
    viewpoint.position = position_interpolator.value_changed;
    // reset orientation
    //motion_setOrientation(motion_startpoint, motion_target);
    rotation_timer.resumeTime = new Date().getTime() / 1000;
}

function startPath(value) {
    if (value === true) {
        if (paused === true) {
            if (is_rotating === true) {
                motion_rotationResume();
            } else {
                motion_movementResume();
            }
        } else {
            motion_startPath();
        }
    }
    paused = false;
}

function stopPath(value) {
    if (value === true) {
        if (is_rotating === true) {
            motion_rotationPause();
        } else {
            motion_movementPause();
        }
        paused = true;
    }
}
