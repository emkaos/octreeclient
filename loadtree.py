from jsontree import JSONTree

import pickle

graphsource = "wikilinks50k.coords.txt"
cachefile = "tree.cache"

def loadTree():
    # try loading cached tree
    try:
        pf = open(cachefile, "r")
        tree = pickle.load(pf)
        print "Loaded tree cache"
        pf.close()
        return tree
    except IOError:
        print "No valid caches found. Building Tree:"

    # load file
    graphfile = open(graphsource, "r")

    maxx = maxy = maxz = 0
    minx = miny = minz = 100000000
    nodes = {}

    lines = []
    # normalize input
    for line in graphfile.readlines():
        items = line.strip().split(";,;")
        items = [item for item in items if item]
        lines.append(items)

    print "Parsed file"

    graphfile.close()

    # add all nodes to dict for edge checks later
    # and calculate bounds of the graph
    for items in lines:
        node = {}
        pos = items[2][1:-1].split(',')
        # XXX better size adjustment
        node['xpos'] = (float(pos[0]) * 1000) - 22000
        node['ypos'] = (float(pos[1]) * 1000) - 22000
        node['zpos'] = (float(pos[2]) * 1000) - 22000
        nodes[items[0]] = node

        if node['xpos'] > maxx: maxx = node['xpos']
        if node['ypos'] > maxy: maxy = node['ypos']
        if node['zpos'] > maxz: maxz = node['zpos']
        if node['xpos'] < minx: minx = node['xpos']
        if node['ypos'] < miny: miny = node['ypos']
        if node['zpos'] < minz: minz = node['zpos']

# build tree
    bounds = (minx, miny, minz, maxx, maxy, maxz)
    print "Treebounds: %s" %  str(bounds)
    tree = JSONTree(bounds , 6)
    
    print "Set up tree"
   
# add nodes and edges to tree
   
    # add nodes
    for key, value in nodes.items():
        tree.insertPoint(key, (value['xpos'], value['ypos'], value['zpos']))
    
    print "Added nodes"

    # add edges with check if corresponding nodes exist
    for items in lines:
        children = items[3:]
        for child in children:
            if child and nodes.has_key(child):
                from_node = nodes[items[0]]
                from_coords = (from_node['xpos'], from_node['ypos'], from_node['zpos'])
                to_node = nodes[child]
                to_coords = (to_node['xpos'], to_node['ypos'], to_node['zpos'])
                tree.insertEdge(from_coords, to_coords)

    print "Added edges"

    print "Tree ready! Added %s points and %s edges" % (tree.root.getPointCount(), tree.root.getEdgeCount())

    tree.createJSON()
    print "Created JSON Caches"
    
    return tree
    print "Writing tree cache"
    pf = open(cachefile, "w")
    pickle.dump(tree, pf)
    pf.close()

    return tree
    

# json layer

def loadJSONTree():
    pass

class JSONOctree:

    def __init__(self, tree):
        self.tree = tree
        
        # cache JSons


if __name__ == '__main__':
    tree = loadTree()
    import pdb;pdb.set_trace() 
