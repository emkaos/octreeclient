QMAP = {'---':0, '+--':1, '-+-':2, '++-':3, '--+':4, '+-+':5, '-++':6, '+++':7}

# XYZ, --- = links vorne unten, +++ = rechts hinten oben 

class OctreeNode:
    branches = [None, None, None, None, None, None, None, None]

    def __init__(self, center, xsize, ysize, zsize, level, is_leaf=False):
        self.is_leaf = is_leaf 
        self.size = (xsize, ysize, zsize)
        self.level = level
        self.point_count = 0
        self.edge_count = 0
        self.points = {}
        self.edges = []
        self.center = center

    def insertEdge(self, from_coords, to_coords):
        self.edges.append((from_coords, to_coords))
        self.edge_count = len(self.edges)

    def insertPoint(self, id, coords):
        self.points[id] = coords
        self.point_count = len(self.points)

    def getColor(self):
        factor =  ((self.size[0]*self.size[1]*self.size[2]) / 1000000000.0) 
        blue = self.getEdgeCount() / factor
        green = self.getPointCount() / factor
        return [0, green, blue]

    def getTransparency(self):
        factor =  ((self.size[0]*self.size[1]*self.size[2]) / 100000000.3) 
        blue = self.getEdgeCount() / factor
        green = self.getPointCount() / factor
        return 1 - blue - green 

    def getPoints(self):
        if self.is_leaf:
            return self.points.values()
        else:
            p = []
            for child in self.branches:
                p.extend(child.getPoints())
            return p

    def getEdges(self):
        if self.is_leaf:
            return self.edges
        else:
            p = []
            for child in self.branches:
                p.extend(child.getEdges())
            return p
        
    def getPointCount(self):
        if self.is_leaf:
            return self.point_count
        else:
            return sum([node.getPointCount() for node in self.branches])

    def getEdgeCount(self):
        # for not leaf nodes this may not represents the number of nodes in the graph
        # as edges that start and end in different sectors are added twice
        if self.is_leaf:
            return self.edge_count
        else:
            return sum([node.getEdgeCount() for node in self.branches])

class Octree:
    root = None
    levels = 0
    bounds = (0,0,0,0,0,0)

    def __init__(self, bounds, depth):
        self.bounds = bounds
        # bounds = (minx, maxx, miny, maxy, minz, maxz )
        self.depth = depth
        
        # build rootnode
        xsize = bounds[3] - bounds[0]
        ysize = bounds[4] - bounds[1]
        zsize = bounds[5] - bounds[2]
        center = (bounds[0] + (xsize / 2), bounds[1] + (ysize / 2), bounds[2] + (zsize / 2) )
        self.root = OctreeNode(center, xsize, ysize, zsize, 1, False)
        self.buildTree(self.root, depth)

    def buildTree(self, root_node, end_level):
        # recusively add tree levels

        def rec(present_node, end_level):
            if present_node.level < end_level:
                branches = self.createBranches(present_node, (present_node.level + 1 >= end_level))
                for child in branches:
                    rec(child, end_level)
                present_node.branches = branches
        
        rec(root_node, end_level)

    def createBranches(self, node, is_leaf=False):
        # calculate child sizes
        xsize = node.size[0] / 2.0
        ysize = node.size[1] / 2.0
        zsize = node.size[2] / 2.0
        # half sizes
        xsize2 = xsize / 2.0
        ysize2 = ysize / 2.0
        zsize2 = zsize / 2.0
        # build all 8 branches
        # --- 
        branches = [None, None, None, None, None, None, None, None]
        center = (node.center[0] - xsize2, node.center[1] - ysize2, node.center[2] - zsize2)
        n = OctreeNode(center, xsize, ysize, zsize, node.level + 1, is_leaf)
        branches[QMAP['---']] = n
        # +-- 
        center = (node.center[0] + xsize2, node.center[1] - ysize2, node.center[2] - zsize2)
        n = OctreeNode(center, xsize, ysize, zsize, node.level + 1, is_leaf)
        branches[QMAP['+--']] = n
        # -+- 
        center = (node.center[0] - xsize2, node.center[1] + ysize2, node.center[2] - zsize2)
        n = OctreeNode(center, xsize, ysize, zsize, node.level + 1, is_leaf)
        branches[QMAP['-+-']] = n
        # ++- 
        center = (node.center[0] + xsize2, node.center[1] + ysize2, node.center[2] - zsize2)
        n = OctreeNode(center, xsize, ysize, zsize, node.level + 1, is_leaf)
        branches[QMAP['++-']] = n
        # --+ 
        center = (node.center[0] - xsize2, node.center[1] - ysize2, node.center[2] + zsize2)
        n = OctreeNode(center, xsize, ysize, zsize, node.level + 1, is_leaf)
        branches[QMAP['--+']] = n
        # +-+ 
        center = (node.center[0] + xsize2, node.center[1] - ysize2, node.center[2] + zsize2)
        n = OctreeNode(center, xsize, ysize, zsize, node.level + 1, is_leaf)
        branches[QMAP['+-+']] = n
        # -++ 
        center = (node.center[0] - xsize2, node.center[1] + ysize2, node.center[2] + zsize2)
        n = OctreeNode(center, xsize, ysize, zsize, node.level + 1, is_leaf)
        branches[QMAP['-++']] = n
        # +++ 
        center = (node.center[0] + xsize2, node.center[1] + ysize2, node.center[2] + zsize2)
        n = OctreeNode(center, xsize, ysize, zsize, node.level + 1, is_leaf)
        branches[QMAP['+++']] = n

        return branches

    def insertPoint(self, id, coords):
        sector = self.findNode(coords, self.depth)
        sector.insertPoint(id, coords)

    def insertEdge(self, start, end):
        sector_start = self.findNode(start, self.depth)
        sector_end = self.findNode(end, self.depth)
        
        sector_start.insertEdge(start, end)

        if sector_start != sector_end:
            sector_end.insertEdge(start, end)

    def findNode(self, point, level):
        # point (x,y,z)
        lvl = 0
        node = self.root
        while lvl < level and not node.is_leaf:
            key = ""
            for i in (0,1,2):
                if point[i] <= node.center[i]:
                    key += "-"
                else:
                    key += "+"
            node = node.branches[QMAP[key]]
            lvl += 1 
        return node

    def getNode(self, number, level):
        # number of sector (see QMAP for order)
        # from 1 to 8
        # every digit one level, last digit first level
        node = self.root
        while level > 0:
            secnum = number % 10
            node = node.branches[secnum - 1] 
            number = number / 10
            level -= 1

        return node
            

    def getJSTree(self):
        # returns tree dict structure with center, size and counts
        root = {}

        def rec(fullnode, copynode):
            # fill node
            copynode['center'] = fullnode.center
            copynode['size'] = fullnode.size
            copynode['point_count'] = fullnode.getPointCount()
            copynode['edge_count'] = fullnode.getEdgeCount()
            if fullnode.is_leaf:
                copynode['branches'] = False
            else:
                copynode['branches'] = [{},{},{},{},{},{},{},{}]
                for i in range(8):
                    rec(fullnode.branches[i],copynode['branches'][i])

        rec(self.root, root) 
        return root
        
