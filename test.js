importScripts("debug.js");

function initialize() {
    server_xhr=new XMLHttpRequest();
    print("start tests");

    for (var i = 0; i < 10000; i++) {
        server_test1();
        server_test2();
    }
    
    print_results("send1");
    print_results("parse1");
    print_results("all2");
}


function server_test1() {
    start_timer("send1");
    server_xhr.open('GET', "http://localhost:8070/1");
    server_xhr.send();
    if (server_xhr.responseText) {
        stop_timer("send1");
        start_timer("parse1");
        var robj = JSON.parse(server_xhr.responseText);
        stop_timer("parse1");
    }

}

function server_test2() {
    start_timer("all2");
    importScripts("http://localhost:8070/2");
    stop_timer("all2");
}
