// Graph specific settings
//
// server url
SETTINGS_server_url = "http://localhost:8070/";

// refresh interval in ms if avatar changed
SETTINGS_refresh_interval = 100;
// force a refresh at this interval. Needed for marked nodes to appear.
SETTINGS_force_refresh_interval = 3000;

// Detail Level 1
SETTINGS_d1_node_size = 100;

SETTINGS_d1_edge_thickness = 3;
SETTINGS_d1_edge_color = new SFColor(0.6,0.6,1);
SETTINGS_d1_edge_transparency = 0.2;

// Detail Level 2
SETTINGS_d2_node_size = 100;
SETTINGS_d2_node_color = new SFColor(0.7,1,0.23);

SETTINGS_d2_edge_thickness = 1;
SETTINGS_d2_edge_color = new SFColor(0.3,0.3,1);
SETTINGS_d2_edge_transparency = 0.7;

// mark nodes
SETTINGS_mark_node_size = 200; 
SETTINGS_mark_node_color = new SFColor(1,0,0);

// neighbor nodes
SETTINGS_neighbor_node_size = 120; 
SETTINGS_neighbor_node_color = new SFColor(1,1,0);
SETTINGS_neighbor_node_transparency = 0.5;

// neighbor links to parent nodes
SETTINGS_neighbor_parent_edge_thickness = 5 ;
SETTINGS_neighbor_parent_edge_color = new SFColor(1,0.7,0);

// neighbor links to child nodes
SETTINGS_neighbor_child_edge_thickness = 6;
SETTINGS_neighbor_child_edge_color = new SFColor(1,1,0);


// Base url for BrowserTexture Interface. Nodename will be appended
//SETTINGS_browser_base_url = "http://de.m.wikipedia.org/wiki/";  // de wikipedia
SETTINGS_browser_base_url = "http://wordnetweb.princeton.edu/perl/webwn?s=";  // Wordnet

// Prefix of the nodename in link urls. Used for finding nodes when links are clicked
SETTINGS_browser_link_prefix = "s="; // Wordnet
//SETTINGS_browser_link_prefix = "/wiki/" // wikipedia

// path speed in meters per seconds
SETTINGS_path_speed = 100;
// path rotation sped in degrees per seconds
SETTINGS_path_rotation_speed = 3;

// speak current path nodename
SETTINGS_speak_path = true;
SETTINGS_speech_language = "en"; // de or en
