function init_controls() {
}

function controls_show(url) {
    controls_browser.url = new MFString(url);
    controls_board.render = true;
    print(url);
}

function closeControls(value) {
    if (value === true) {
        controls_board.render = false;
    } 
}

function leftControls(value) {
    if (value === true) {
        left_node = last_select;
        left_select_text.string = new MFString(last_select_name); 
    }
}

function rightControls(value) {
    if (value === true) {
        right_node = last_select;
        right_select_text.string = new MFString(last_select_name); 
    } 
}

function searchPath(value) {
    if (value === true) {
    server_searchPath(left_node, right_node);
    } 
}

function jumpControls(value) {
    if (value === true) {
        jump_node = last_select;
        server_findNode(jump_node);
    } 
}

function focusControls(value) {
    if (value === true) {
        var nodeid = last_select;
        server_focusNode(nodeid); 
    } 
}

function mousePicked(value) {
    if (value === true) {
        var g = pointer.hitObject_changed;
        last_select = g.nodeid;
        server_queryNode(g.nodeid);
    } 
}

function mousePicked2(value) {
    if (value === true) {
        var g = pointer2.hitObject_changed;
        last_select = g.nodeid;
        server_queryNode(g.nodeid);
    } 
}

function mousePicked3(value) {
    if (value === true) {
        var g = pointer4.hitObject_changed;
        last_select = g.nodeid;
        server_queryNode(g.nodeid);
    } 
}

function neighborPicked(value) {
    if (value === true) {
        var g = pointer3.hitObject_changed;
        server_queryNode(g.nodeid);
    } 
}

function setExplore(value) {
    if (value === true) {
        server_send_reset();
        setModeExplore();
        cameraChanged();
    } 
}

function searchPicked(value) {
    if (value === true) {
        g = search_pointer.hitObject_changed;
        server_findNode(g.nodeid);
        setModeExplore();
    } 
} 

function worldClicked(value) {
    if (value === true) {
        if (mode == "search") {
            setModeExplore();
        } else if (mode == "explore") {
            setModeNeighbors();
        } else if (mode == "neighbors") {
            setModeExplore();
        }
    } 
} 

function browserLinkChanged(value) {
    controls_browser.url = new MFString(value);
    if (value.substring(0,SETTINGS_browser_base_url.length) == SETTINGS_browser_base_url)
        print(value.search(SETTINGS_browser_link_prefix));
        var nodename = value.substring(value.search(SETTINGS_browser_link_prefix) + SETTINGS_browser_link_prefix.length);
        print("nodename:" +  nodename);
        server_findNodeByName(nodename);
}
