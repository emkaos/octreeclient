var visibility_tanc;

function init_visibility(va) {
    visibility_tanc = Math.tan(fova);
}

function visibility_pointOnLOS(v) {
    var x = v.subtract(current_position).dot(current_los) / (current_los.dot(current_los));
    var d = current_position.add(current_los.multiply(x));
    return d;
}

function visibility_isVisible(node){ 
    start_timer("visibility");
    // check if behind 

    var v = SFVec3f(node.center[0], node.center[1], node.center[2]);
    var vd = v.subtract(current_position);
    var sd = Math.sqrt(Math.pow(node.size[0] / 2 ,2) + Math.pow(node.size[1] / 2,2) + Math.pow(node.size[2] / 2,2)); // cube diagonal

    if (vd.dot(current_los) < 0 && vd.length() > sd) {
        return null;
    }

    var p = visibility_pointOnLOS(v);  // nearest point on los from node center
    var op = current_position.subtract(p);
    var pd = Math.sqrt(Math.pow(p[0] - node.center[0], 2) + Math.pow(p[1] - node.center[1], 2) + Math.pow(p[2] - node.center[2] ,2)); // distance from p to node center
    var scr = visibility_tanc * op.length(); // view radius at point p

    // early sort out if distance to los greater than space diagonal plus cube radius
    stop_timer("visibility");
    if (scr  <  pd - sd)  { 
        return null; 
    }
    else { 
        return op.length() -sd;
    }

    // case1 los goes through node 

    // case2 node edges lie in view cone

    // returns distance on los 
}

function visibility_gatherVisibleNodes() {
    var lvl6_cutoff = 20000;
    var lvl5_cutoff = 30000;
    var lvl4_cutoff = 40000;
    var lvl3_cutoff = 80000;
    var lvl2_cutoff = 150000;
    var lvl1_cutoff = 500000;

    var root = tree_Octree;
    for (var i1=0; i1 < 8; i1++) { // first tree level
        var node = root.branches[i1];
        var d1 = visibility_isVisible(node);
        if (d1 && d1 < lvl1_cutoff) {

            if (d1 < lvl2_cutoff) { // second level
                for (var i2=0; i2 < 8; i2++) { 
                    var node2 = node.branches[i2];
                    var d2 = visibility_isVisible(node2);

                    if (d2 < lvl3_cutoff) { // third level level
                        for (var i3=0; i3 < 8; i3++) { // first tree level
                            var node3 = node2.branches[i3];
                            var d3 = visibility_isVisible(node3);

                            if (d3 < lvl4_cutoff ) {
                                for (var i4=0; i4 < 8; i4++) { // first tree level
                                    var node4 = node3.branches[i4];

                                    if (node4.point_count > 0) {
                                        var d4 = visibility_isVisible(node4);
                                        
                                        if (d4 < lvl5_cutoff) {
                                            for (var i5=0; i5 < 8; i5++) {
                                                var node5 = node4.branches[i5];
                                                var d5 = visibility_isVisible(node5);
                                                if (node5.point_count > 0) {
                                                    if (d5 < lvl6_cutoff) {
                                                    //nodefactory_queueUltraLowResLocal(node5);
                                                    server_queueLowRes(5,i1 + i2 * 10 + i3 * 100 + i4 * 1000 + i5 *10000); 
                                                    } else {
                                                    //nodefactory_queueBillboard(node5);
                                                    nodefactory_queueUltraLowResLocal(node5);
                                                                }
                                                }
                                            }
                                        } else {
                                            nodefactory_queueUltraLowResLocal(node4);
                                            //server_queueLowRes(4,i1 + i2 * 10 + i3 * 100 + i4 * 1000); 
                                        }
                                    }
                                }
                            } else {
                                nodefactory_queueUltraLowResLocal(node3);
                            }
                        }

                    } else {
                        nodefactory_queueUltraLowResLocal(node2);
                    }
                }

            } else {
                nodefactory_queueUltraLowResLocal(node);
            }
        }
    }
}
