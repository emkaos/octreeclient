var nodefactory_to_add = {};
var nodefactory_bin = {};
var nodefactory_id_node_map_d1 = {}; // low res
var nodefactory_id_node_map_d2 = {}; // ultra low res
var nodefactory_id_node_map_d3 = {}; // high res

function init_nodefactory() {
    nodefactory_to_add[5] = MFNode(); // high res edges
    nodefactory_to_add[4] = MFNode(); // high res nodes
    nodefactory_to_add[3] = MFNode(); // ultra low res nodes
    nodefactory_to_add[2] = MFNode(); // low res edges
    nodefactory_to_add[1] = MFNode(); // low res nodes
    nodefactory_to_add[0] = MFNode(); // debug nodes
    nodefactory_bin[12] = []; // high res edges
    nodefactory_bin[11] = []; // high res nodes
    nodefactory_bin[10] = []; // high res groups
    nodefactory_bin[30] = []; // ultra low res
    nodefactory_bin[22] = []; // low res edges
    nodefactory_bin[21] = []; // low res nodes
    nodefactory_bin[20] = []; // low res groups
}

// delete detail level 3 nodes from scene and put into trash
function nodefactory_trashULR(cells){
    if (cells == "ALL") {
        for (var i=0;i<ultralowres.children.length;i++) {
            nodefactory_bin[30].push(ultralowres.children[i]);
        }
        nodefactory_id_node_map_d2 = {};
        ultralowres.children = new MFNode();
        //print("ulr remove all");
    } else {
        var to_remove = new MFNode();
        for (i=0; i<cells.length;i++) {
            var id = cells[i];
            //print(cells[i]);
            var node = nodefactory_id_node_map_d2[id];
            if (node == undefined || node === false) { // needed to handle out of order response handling
                print("Found undefined node for "+id);
                //1;
            } else {
                nodefactory_bin[30].push(node);
                nodefactory_id_node_map_d2[id] = false;
                to_remove[to_remove.length] = node;
            }
            //nodefactory_id_node_map_d2[id] = false;
        }
       // print("ulr remove " + to_remove.length);
        ultralowres.removeChildren = to_remove;
    }
   // print("nodemap length:" + Object.keys(nodefactory_id_node_map_d2).length);
}

// delete detail level 2 nodes from scene and put into trash
function nodefactory_trashLR(cells) {
    if (cells == "ALL") {
        for (var i=0;i<lowres_edges.children.length;i++) {
            nodefactory_bin[22].push(lowres_edges.children[i]);
        }
        // first node is touch sensor
        for (i=1;i<lowres_nodes.children.length;i++) {
            var nodegroup = lowres_nodes.children[i];
            for (var j=0;j<nodegroup.children.length;j++) {
                if (nodegroup.children[j]) {
                    nodefactory_bin[21].push(nodegroup.children[j]);
                }
            }
            nodegroup.children = new MFNode();
            nodefactory_bin[20].push(nodegroup);
        }
        nodefactory_id_node_map_d1 = {};
        lowres_edges.children = new MFNode();
        lowres_nodes.children = new MFNode();
        lowres_nodes.children[0] = pointer;
    } else {
        var to_remove_edges = new MFNode();
        var to_remove_nodes = new MFNode();
        for (i=0;i<cells.length;i++) {
            var id = cells[i];
            if (nodefactory_id_node_map_d1[id]) {
                var edge = nodefactory_id_node_map_d1[id][1];
                nodegroup = nodefactory_id_node_map_d1[id][0];
                //print(edge);
                //print(nodegroup);

                nodefactory_bin[22].push(edge);
                to_remove_edges[to_remove_edges.length] = edge;

                for (j=0; j<nodegroup.children.length;j++) {
                    if (nodegroup.children[j]) {
                        nodefactory_bin[21].push(nodegroup.children[j]);    
                    }
                }
                nodegroup.children = new MFNode();
                nodefactory_bin[20].push(nodegroup);
                to_remove_nodes[to_remove_nodes.length] = nodegroup;

                nodefactory_id_node_map_d1[id] = false;
            }
        }
        lowres_edges.removeChildren = to_remove_edges;
        lowres_nodes.removeChildren = to_remove_nodes;
    }
}

// delete detail level 1 nodes from scene and put into trash
function nodefactory_trashHR(cells) {
    if (cells == "ALL") {
        for (var i=0;i<highres_edges.children.length;i++) {
            nodefactory_bin[12].push(highres_edges.children[i]);
        }
        // first node is touch sensor
        for (i=1;i<highres_nodes.children.length;i++) {
            var nodegroup = highres_nodes.children[i];
          //  print(nodegroup);
            if (nodegroup !== null) {
                for (var j=0;j<nodegroup.children.length;j++) {
                    if (nodegroup.children[j]) {
                        nodefactory_bin[11].push(nodegroup.children[j]);
                    }
                }
                nodegroup.children = new MFNode();
                nodefactory_bin[10].push(nodegroup);
            }
        }
        nodefactory_id_node_map_d3 = {};
        highres_edges.children = new MFNode();
        highres_nodes.children = new MFNode();
        highres_nodes.children[0] = pointer2;
    } else {
        var to_remove_edges = new MFNode();
        var to_remove_nodes = new MFNode();
        for (i=0;i<cells.length;i++) {
            var id = cells[i];
            if (nodefactory_id_node_map_d3[id]) {
                var edge = nodefactory_id_node_map_d3[id][1];
                nodegroup = nodefactory_id_node_map_d3[id][0];
                //print(edge);
                //print(nodegroup);

                nodefactory_bin[12].push(edge);
                to_remove_edges[to_remove_edges.length] = edge;

                for (j=0; j<nodegroup.children.length;j++) {
                    if (nodegroup.children[j]) {
                        nodefactory_bin[11].push(nodegroup.children[j]);    
                    }
                }
                nodegroup.children = new MFNode();
                nodefactory_bin[10].push(nodegroup);
                to_remove_nodes[to_remove_nodes.length] = nodegroup;

                nodefactory_id_node_map_d3[id] = false;
            }
        }
        highres_edges.removeChildren = to_remove_edges;
        highres_nodes.removeChildren = to_remove_nodes;
    }
}


// add all queued nodes to scene
function nodefactory_addAllInQueue() {
    ultralowres.addChildren = nodefactory_to_add[3];
    lowres_edges.addChildren = nodefactory_to_add[2];
    lowres_nodes.addChildren = nodefactory_to_add[1];
    highres_edges.addChildren = nodefactory_to_add[5];
    highres_nodes.addChildren = nodefactory_to_add[4];
    debug_group.children = nodefactory_to_add[0];
    nodefactory_to_add[5] = MFNode();
    nodefactory_to_add[4] = MFNode();
    nodefactory_to_add[3] = MFNode();
    nodefactory_to_add[2] = MFNode();
    nodefactory_to_add[1] = MFNode();
    nodefactory_to_add[0] = MFNode();
}

// get detail level 3 node from tashbin and set attributes
function nodefactory_recycleUltraLowResNode(data) {
    var transparency = data[10];

    var transform = nodefactory_bin[30].pop(); 
    transform.translation = new SFVec3f(data[1], data[2], data[3]);
    var shape = transform.getChildren()[0];
    shape.geometry.size = new SFVec3f(data[4], data[5], data[6]);
    shape.appearance.material.emissiveColor = new SFColor(data[7], data[8], data[9]);
    shape.appearance.material.transparency = new SFFloat(transparency);

    return transform;
}

// create new detail level 3 node
function nodefactory_createUltraLowResNode(data) {
    var transparency = data[10];

    var box = Browser.currentScene.createNode("Box");
    box.size = new SFVec3f(data[4], data[5], data[6]);

    var material = Browser.currentScene.createNode("Material");
    material.emissiveColor = new SFColor(data[7], data[8], data[9]);
    material.transparency = new SFFloat(transparency);

    var appearance = Browser.currentScene.createNode("Appearance");
    appearance.material = material;

    var shape = Browser.currentScene.createNode("Shape");
    shape.geometry = box;
    shape.appearance = appearance;

    var transform = Browser.currentScene.createNode("Transform");

    transform.translation = new SFVec3f(data[1], data[2], data[3]);
    transform.children = new MFNode(shape);

    return transform;
}

// create or recycle detail level 3 node
function nodefactory_queueUltraLowRes(data) {
    var transform;
    if (nodefactory_bin[30].length > 0) {
        transform = nodefactory_recycleUltraLowResNode(data);
    } else {
        transform = nodefactory_createUltraLowResNode(data);
    }
    //print("SET "+data[0]);
    nodefactory_id_node_map_d2[data[0]] = transform;
    nodefactory_to_add[3][nodefactory_to_add[3].length] = transform;
}

// create or recycle detail level 2 node
function nodefactory_queueLowRes(cells) {
    // Nodes

    // debug
   //var cc = 0;
   //var rc = 0;
   //var ac = 0;

   // loop cells
   for (var i=0; i<cells.length;i++) {
        var nodes = cells[i][1];
        var edges = cells[i][2];
        
        var group;
        if (nodefactory_bin[20].length>0) {
            group = nodefactory_bin[20].pop(); 
        } else {
            group = Browser.currentScene.createNode("Group");
        }
        var childnodes = new MFNode();
        var nodecount = nodes.length / 4;

       // ac += nodecount;
        for (var n=0; n < nodecount; n++) {
            if (nodefactory_bin[21].length > 0) {
                transform = nodefactory_recycleLowResNode(nodes[n*4+1], nodes[n*4+2], nodes[n*4+3], nodes[n*4]);
                //rc++;
            } else {
                //cc++;
                transform = nodefactory_createLowResNode(nodes[n*4+1], nodes[n*4+2], nodes[n*4+3], nodes[n*4]);
            }
            childnodes[childnodes.length] = transform;
        }
        group.children = childnodes;
        nodefactory_to_add[1][nodefactory_to_add[1].length] = group;
        var shape;
        
        if (nodefactory_bin[22].length > 0) {
            shape = nodefactory_recycleLowResEdges(edges);
        } else {
            shape = nodefactory_createLowResEdges(edges);
        }
        nodefactory_to_add[2][nodefactory_to_add[2].length] = shape;
        
        nodefactory_id_node_map_d1[cells[i][0]] = [group, shape];
   }
   //print("AC: " + ac + " CC: " + cc + " RC: " + rc);

}

// get detail level 2 edges from trashbin and set attributes
function nodefactory_recycleLowResEdges(data) {
    var shape = nodefactory_bin[22].pop();
    var ilineset = shape.geometry;
    var edgecount = data.length / 6;

    var points = new MFVec3f(); 
    var indexes = MFInt32();

    for (var i=0; i < edgecount; i++) {
        points[i*2] = new SFVec3f(data[i*6], data[i*6+1], data[i*6+2]);
        points[(i*2)+1] = new SFVec3f(data[i*6+3], data[i*6+4], data[i*6+5]);
        indexes[(i*3)] = i*2;
        indexes[(i*3)+1] = i*2+1;
        indexes[(i*3)+2] = -1;
    }

    ilineset.coord.point = points;
    ilineset.coordIndex = indexes;
    return shape;
}

// create new detail level 2 edges
function nodefactory_createLowResEdges(data) {
    var edgecount = data.length / 6;

    // Add edges
    var shape = Browser.currentScene.createNode("Shape");
    var appearance = Browser.currentScene.createNode("Appearance");
    var material = Browser.currentScene.createNode("Material");
    material.emissiveColor = SETTINGS_d2_edge_color;
    material.transparency = new SFFloat(SETTINGS_d2_edge_transparency);
    var lineproperties = Browser.currentScene.createNode("LineProperties");
    lineproperties.linewidthScaleFactor = new SFFloat(SETTINGS_d2_edge_thickness);
    appearance.material = material;
    appearance.lineProperties = lineproperties;
    shape.appearance = appearance;
    var ilineset = Browser.currentScene.createNode("IndexedLineSet");

    points = new MFVec3f(); 
    indexes = new MFInt32();
    for (var i=0; i < edgecount; i++) {
        points[i*2] = new SFVec3f(data[i*6], data[i*6+1], data[i*6+2]);
        points[(i*2)+1] = new SFVec3f(data[i*6+3], data[i*6+4], data[i*6+5]);
        indexes[(i*3)] = i*2;
        indexes[(i*3)+1] = i*2+1;
        indexes[(i*3)+2] = -1;
    }

    var coord  = Browser.currentScene.createNode("Coordinate");
    coord.point = points; 

    ilineset.coordIndex = indexes;
    ilineset.coord = coord;
    shape.addChild(ilineset);
    shape.addChild(ilineset);
    return shape;
} 

// create new detail level 2 node
function nodefactory_createLowResNode(x, y, z, id) {

    var box = Browser.currentScene.createNode("Box");
    var s = SETTINGS_d2_node_size;
    box.size = new SFVec3f(s,s,s);

    var material = Browser.currentScene.createNode("Material");
    material.diffuseColor = new SFColor(0.7,1,0.23);

    var appearance = Browser.currentScene.createNode("Appearance");
    appearance.material = material;

    var shape = Browser.currentScene.createNode("Shape");
    shape.geometry = box;
    shape.appearance = appearance;
    shape.nodeid = id;

    var transform = Browser.currentScene.createNode("Transform");
    transform.translation = new SFVec3f(x, y, z);
    transform.children = new MFNode(shape);

    return transform;
}

// get detail level 2 node from trashbin and set attributes
function nodefactory_recycleLowResNode(x,y,z, id) {
    var transform = nodefactory_bin[21].pop(); 
    transform.translation = new SFVec3f(x, y, z);
    var shape = transform.getChildren()[0];
    shape.nodeid = id;
    return transform;
}

// create or recycle detail level 1 nodes
function nodefactory_queueHighRes(cells) {
    // Nodes

   for (var i=0; i<cells.length;i++) {
        var nodes = cells[i][1];
        var edges = cells[i][2];
        
        var group;
        if (nodefactory_bin[10].length>0) {
            group = nodefactory_bin[10].pop(); 
        } else {
            group = Browser.currentScene.createNode("Group");
        }
        var childnodes = new MFNode();
        var nodecount = nodes.length / 5;
        for (var n=0; n < nodecount; n++) {
            if (nodefactory_bin[11].length > 0) {
                transform = nodefactory_recycleHighResNode(nodes[n*5+1], nodes[n*5+2], nodes[n*5+3], nodes[n*5], nodes[n*5+4]);
            } else {
                transform = nodefactory_createHighResNode(nodes[n*5+1], nodes[n*5+2], nodes[n*5+3], nodes[n*5], nodes[n*5+4]);
            }
            childnodes[childnodes.length] = transform;
        }
        group.children = childnodes;
        nodefactory_to_add[4][nodefactory_to_add[4].length] = group;

        var shape;
        if (nodefactory_bin[12].length > 0) {
            shape = nodefactory_recycleHighResEdges(edges);
        } else {
            shape = nodefactory_createHighResEdges(edges);
        }
        nodefactory_to_add[5][nodefactory_to_add[5].length] = shape;

        nodefactory_id_node_map_d3[cells[i][0]] = [group, shape];
   }
}

// get detail level 1 edge node from trashbin and set attributes
function nodefactory_recycleHighResEdges(data) {
    var shape = nodefactory_bin[12].pop();
    var ilineset = shape.geometry;
    var edgecount = data.length / 6;

    var points = new MFVec3f(); 
    var indexes = MFInt32();

    for (var i=0; i < edgecount; i++) {
        points[i*2] = new SFVec3f(data[i*6], data[i*6+1], data[i*6+2]);
        points[(i*2)+1] = new SFVec3f(data[i*6+3], data[i*6+4], data[i*6+5]);
        indexes[(i*3)] = i*2;
        indexes[(i*3)+1] = i*2+1;
        indexes[(i*3)+2] = -1;
    }

    ilineset.coord.point = points;
    ilineset.coordIndex = indexes;
    return shape;
}

// create new detail level 1 edge node
function nodefactory_createHighResEdges(data) {
    var edgecount = data.length / 6;

    // Add edges
    var shape = Browser.currentScene.createNode("Shape");
    var appearance = Browser.currentScene.createNode("Appearance");
    var material = Browser.currentScene.createNode("Material");
    material.emissiveColor = SETTINGS_d1_edge_color;
    material.transparency = new SFFloat(SETTINGS_d1_edge_transparency);
    var lineproperties = Browser.currentScene.createNode("LineProperties");
    lineproperties.linewidthScaleFactor = new SFFloat(SETTINGS_d1_edge_thickness);
    appearance.material = material;
    appearance.lineProperties = lineproperties;
    shape.appearance = appearance;
    var ilineset = Browser.currentScene.createNode("IndexedLineSet");

    points = new MFVec3f(); 
    indexes = new MFInt32();
    for (var i=0; i < edgecount; i++) {
        points[i*2] = new SFVec3f(data[i*6], data[i*6+1], data[i*6+2]);
        points[(i*2)+1] = new SFVec3f(data[i*6+3], data[i*6+4], data[i*6+5]);
        indexes[(i*3)] = i*2;
        indexes[(i*3)+1] = i*2+1;
        indexes[(i*3)+2] = -1;
    }

    var coord  = Browser.currentScene.createNode("Coordinate");
    coord.point = points; 

    ilineset.coordIndex = indexes;
    ilineset.coord = coord;
    shape.addChild(ilineset);
    shape.addChild(ilineset);
    return shape;
} 

// create new detail level 1 node node
function nodefactory_createHighResNode(x, y, z, id, image) {

    var box = Browser.currentScene.createNode("Box");
    var s = SETTINGS_d1_node_size;
    box.size = new SFVec3f(s,s,s);

    /*
    var material = Browser.currentScene.createNode("Material");
    material.diffuseColor = new SFColor(0.7,1,0.23);
    */

    var imagetexture = Browser.currentScene.createNode("ImageTexture");
    if (image !== "") {
        //print(image);
        urls = new MFString(); 
        urls[1] = new SFString("http://de.wikipedia.org/w/thumb.php?w=200&f=" + image);
        urls[0] = new SFString("http://commons.wikimedia.org/w/thumb.php?w=200&f=" + image);
        imagetexture.url = urls;
    }

    var appearance = Browser.currentScene.createNode("Appearance");
    appearance.texture = imagetexture;

    var shape = Browser.currentScene.createNode("Shape");
    shape.geometry = box;
    shape.appearance = appearance;
    shape.nodeid = id;

    var transform = Browser.currentScene.createNode("Transform");
    transform.translation = new SFVec3f(x, y, z);
    transform.children = new MFNode(shape);

    return transform;
}

// get detail level 1 node from trashbin and set attributes
function nodefactory_recycleHighResNode(x,y,z, id, image) {
    var transform = nodefactory_bin[11].pop(); 
    transform.translation = new SFVec3f(x, y, z);
    var shape = transform.getChildren()[0];
    shape.nodeid = id;
    if (image !== "") {
        //print(image);
        var imagetexture = shape.appearance.texture;
        urls = new MFString(); 
        urls[1] = new SFString("http://de.wikipedia.org/w/thumb.php?w=200&f=" + image);
        urls[0] = new SFString("http://commons.wikimedia.org/w/thumb.php?w=200&f=" + image);
        imagetexture.url = urls;
    }
    return transform;
}

function nodefactory_queueDebugCell(data, r, g, b) {
    nodefactory_to_add[0][nodefactory_to_add[0].length] = nodefactory_createDebugCell(data, r, g, b);
}

// create a new debug node
function nodefactory_createDebugCell(data, r, g, b) {
    var shape = Browser.currentScene.createNode("Shape");
    var appearance = Browser.currentScene.createNode("Appearance");
    var material = Browser.currentScene.createNode("Material");
    material.emissiveColor = new SFColor(r,g,b);
    var lineproperties = Browser.currentScene.createNode("LineProperties");
    lineproperties.linewidthScaleFactor = new SFFloat(1);
    appearance.material = material;
    appearance.lineProperties = lineproperties;
    shape.appearance = appearance;
    var ilineset = Browser.currentScene.createNode("IndexedLineSet");

    var points = new MFVec3f(); 
    sindexes = [0,1,2,3,0,4,5,6,7,4,-1,1,5,-1,2,6,-1,3,7,-1];
    var indexes = new MFInt32();
    for (var i=0; i<sindexes.length; i++) {
        indexes[i] = sindexes[i];
    }
    x = data[0];
    y = data[1];
    z = data[2];
    xw = data[3];
    yw = data[4];
    zw = data[5];
    
    points[0] = new SFVec3f(x - xw, y-yw, z-zw);
    points[1] = new SFVec3f(x + xw, y-yw, z-zw);
    points[2] = new SFVec3f(x + xw, y-yw, z+zw);
    points[3] = new SFVec3f(x - xw, y-yw, z+zw);
    points[4] = new SFVec3f(x - xw, y+yw, z-zw);
    points[5] = new SFVec3f(x + xw, y+yw, z-zw);
    points[6] = new SFVec3f(x + xw, y+yw, z+zw);
    points[7] = new SFVec3f(x - xw, y+yw, z+zw);

    var coord  = Browser.currentScene.createNode("Coordinate");
    coord.point = points; 

    ilineset.coordIndex = indexes;
    ilineset.coord = coord;
    shape.addChild(ilineset);
    //shape.addChild(ilineset);
    return shape;
} 

function nodefactory_createNeighborView(data) {
    print("creating neighbor view");
    nodefactory_trashAllNeighborView();

    var nodes = new MFNode();
    var center = data[0];
    var children = data[1];
    var parents = data[2];

    var centernode = nodefactory_createNeighborNode(center[1], center[2], center[3], center[0], center[4]);
    nodes[0] = pointer3;
    nodes[1] = centernode;
    
    // children
    for (var i=0; i<children.length;i++) {
        var child = children[i];
        nodes[nodes.length] = nodefactory_createNeighborNode(child[1], child[2], child[3], child[0], child[4]);
    }

    // parents
    for (i=0; i<parents.length;i++) {
        var parent = parents[i];
        nodes[nodes.length] = nodefactory_createNeighborNode(parent[1], parent[2], parent[3], parent[0], parent[4]);
    }

    var linesets = nodefactory_createNeighborEdges(center, children, parents);
    nodes[nodes.length] = linesets[0];
    nodes[nodes.length] = linesets[1];
    neighbors_group.children = nodes;
    
    //setModeNeighbors();
    //server_findNode(center[0]);
}

function nodefactory_createNeighborEdges(center, children, parents) {
    edgesets = new MFNode();
    
    // links to children
    var shape = Browser.currentScene.createNode("Shape");
    var appearance = Browser.currentScene.createNode("Appearance");
    var material = Browser.currentScene.createNode("Material");
    material.emissiveColor = SETTINGS_neighbor_child_edge_color;
    var lineproperties = Browser.currentScene.createNode("LineProperties");
    lineproperties.linewidthScaleFactor = new SFFloat(SETTINGS_neighbor_child_edge_thickness);
    appearance.material = material;
    appearance.lineProperties = lineproperties;
    shape.appearance = appearance;
    var ilineset = Browser.currentScene.createNode("IndexedLineSet");

    points = new MFVec3f(); 
    indexes = new MFInt32();
    points[0] = new SFVec3f(center[1], center[2], center[3]);
    for (var i=0; i < children.length; i++) {
        var child = children[i];
        points[i+1] = new SFVec3f(child[1], child[2], child[3]);

        indexes[(i*3)] = 0;
        indexes[(i*3)+1] = i+1;
        indexes[(i*3)+2] = -1;
    }

    var coord  = Browser.currentScene.createNode("Coordinate");
    coord.point = points; 

    ilineset.coordIndex = indexes;
    ilineset.coord = coord;
    shape.addChild(ilineset);

    edgesets[0] = shape;
    //shape.addChild(ilineset);

    // links to parents
    shape = Browser.currentScene.createNode("Shape");
    appearance = Browser.currentScene.createNode("Appearance");
    material = Browser.currentScene.createNode("Material");
    material.emissiveColor = SETTINGS_neighbor_parent_edge_color;
    lineproperties = Browser.currentScene.createNode("LineProperties");
    lineproperties.linewidthScaleFactor = new SFFloat(SETTINGS_neighbor_parent_edge_thickness);
    appearance.material = material;
    appearance.lineProperties = lineproperties;
    shape.appearance = appearance;
    ilineset = Browser.currentScene.createNode("IndexedLineSet");

    points = new MFVec3f(); 
    indexes = new MFInt32();
    points[0] = new SFVec3f(center[1], center[2], center[3]);
    for (i=0; i < parents.length; i++) {
        parent = parents[i];
        points[i+1] = new SFVec3f(parent[1], parent[2], parent[3]);

        indexes[(i*3)] = 0;
        indexes[(i*3)+1] = i+1;
        indexes[(i*3)+2] = -1;
    }

    coord  = Browser.currentScene.createNode("Coordinate");
    coord.point = points; 

    ilineset.coordIndex = indexes;
    ilineset.coord = coord;
    shape.addChild(ilineset);

    edgesets[1] = shape;
    return edgesets;


    // links to parents
}

function nodefactory_createNeighborNode(x,y,z, id, name) {
    var box = Browser.currentScene.createNode("Box");
    var s = SETTINGS_neighbor_node_size;
    box.size = new SFVec3f(s, s, s);

    var material = Browser.currentScene.createNode("Material");
    material.emissiveColor = SETTINGS_neighbor_node_color;
    material.ambientIntensity = new SFFloat(0.2);
    material.transparency = new SFFloat(SETTINGS_neighbor_node_transparency);

    var appearance = Browser.currentScene.createNode("Appearance");
    appearance.material = material;

    var shape = Browser.currentScene.createNode("Shape");
    shape.geometry = box;
    shape.appearance = appearance;
    shape.nodeid = id;

    var transform = Browser.currentScene.createNode("Transform");
    transform.translation = new SFVec3f(x, y, z);

    var transform2 = Browser.currentScene.createNode("Transform");
    transform2.translation = new SFVec3f(SETTINGS_neighbor_node_size + 10, -10, 0);
    var shape2 = Browser.currentScene.createNode("Shape");
    var text = Browser.currentScene.createNode("SolidText");
    var fontstyle = Browser.currentScene.createNode("FontStyle");
    fontstyle.size = new SFFloat(SETTINGS_neighbor_node_size);
    fontstyle.family = new MFString("SANS");
    text.fontStyle = fontstyle;

    var material2 = Browser.currentScene.createNode("Material");
    material2.emissiveColor = SETTINGS_neighbor_node_color;
    material2.ambientIntensity = new SFFloat(0.2);

    var appearance2 = Browser.currentScene.createNode("Appearance");
    appearance2.material = material2;

    shape2.geometry = text;
    shape2.appearance = appearance2;
    shape2.nodeid = id;
    text.string = new MFString(name);
    transform2.children = new MFNode(shape2);

    var children = new MFNode(shape);
    children[1] = transform2;
    transform.children = children;

    return transform;
}

function nodefactory_trashAllNeighborView() {
    neighbors_group.children = new MFNode();
}

function nodefactory_createPathView(data) {
    print("creating neighbor view");
    nodefactory_trashAllNeighborView();

    var nodes = new MFNode();
    nodes[0] = pointer3;

    for (var i=0; i<data.length; i++) {
        var node = data[i];
        nodes[nodes.length] = nodefactory_createNeighborNode(node[1], node[2], node[3], node[0], node[4]);
    }

    var lineset = nodefactory_createPathEdges(data);
    nodes[nodes.length] = lineset;
    neighbors_group.children = nodes;
    
    //setModeNeighbors();
   // server_findNode(data[0][0]);
}

function nodefactory_createPathEdges(data) {
    var shape = Browser.currentScene.createNode("Shape");
    var appearance = Browser.currentScene.createNode("Appearance");
    var material = Browser.currentScene.createNode("Material");
    material.emissiveColor = new SFColor(0.8,0.2,1);
    material.ambientIntensity = new SFFloat(1);
    var lineproperties = Browser.currentScene.createNode("LineProperties");
    lineproperties.linewidthScaleFactor = new SFFloat(5);
    appearance.material = material;
    appearance.lineProperties = lineproperties;
    shape.appearance = appearance;
    var ilineset = Browser.currentScene.createNode("IndexedLineSet");

    points = new MFVec3f(); 
    indexes = new MFInt32();
    for (var i=0; i < data.length; i++) {
        
        var node = data[i];
        points[i] = new SFVec3f(node[1], node[2], node[3]);

        if (i > 0) {
            indexes[((i-1)*3)] = i-1;
            indexes[((i-1)*3)+1] = i;
            indexes[((i-1)*3)+2] = -1;
        }
    }
    coord  = Browser.currentScene.createNode("Coordinate");
    coord.point = points; 

    ilineset.coordIndex = indexes;
    ilineset.coord = coord;
    shape.addChild(ilineset);
    
    return shape;
}

function nodefactory_markNode(data) {

    var box = Browser.currentScene.createNode("Box");
    var s = SETTINGS_mark_node_size;
    box.size = new SFVec3f(s, s, s);

    var appearance = Browser.currentScene.createNode("Appearance");
    var material = Browser.currentScene.createNode("Material");
    material.emissiveColor = SETTINGS_mark_node_color;
    material.ambientIntensity = new SFFloat(1);
    material.shininess = new SFFloat(1);
    appearance.material = material;

    var shape = Browser.currentScene.createNode("Shape");
    shape.geometry = box;
    shape.appearance = appearance;
    shape.nodeid = data[0];

    var transform = Browser.currentScene.createNode("Transform");
    transform.nodeid = data[0];
    transform.translation = new SFVec3f(data[1], data[2], data[3]);
    transform.children = new MFNode(shape);

    Browser.addRoute( blink_sensor, 'fraction_changed', material, 'transparency');
    mark_group.addChildren = new MFNode(transform);
}

function nodefactory_unmarkNode(data) {
    var toremove = new MFNode();
    var tf;
    for (var i=0; i<mark_group.children.length; i++) {
        tf = mark_group.children[i];
        if (tf.nodeid == data) {
            toremove[toremove.length] = tf;
        }
    }
    mark_group.removeChildren = toremove;
}

