function init_search() {
}


// start searching for node
function search_searchString(value) {
    //show_world();
    search_board.render = false;
    if (value && value.length > 0) { 
        mode = "search";
        server_searchNode(value);
    }
}

/* display search string */
function search_stringInput(value) {
    if (value && value.length > 0) { 
        search_board.render = true;
        hide_graph();
        top_world.render = true;
        search_board.geometry.string = new MFString(value);
    } 
}


function search_showResults(data) {

    if (data.length > 0) {
        nodefactory_trashULR("ALL");
        nodefactory_trashLR("ALL");
        var nodes = new MFNode();

        for (var i=0; i< data.length; i++) {
            var z = -50 + ~~(i / 7)*-40; 
            var y = 7 - (i % 7)*2; 
            
            var node = search_createResultNode(data[i][0], data[i][1], -20, y, z);
            nodes[nodes.length] = node;
        } 
        var transform = search_results.children[0];
        transform.translation = current_position;
        transform.rotation = current_orientation;
        transform.children = new MFNode();
        transform.addChildren = nodes;
        
        setModeSearch();
    }
}


function search_createResultNode(name, id,  x, y, z) {
    var box = Browser.currentScene.createNode("Box");
    box.size = new SFVec3f(0.5,0.5,0.5);

    var material = Browser.currentScene.createNode("Material");
    material.emissiveColor = new SFColor(1,1,1);

    var appearance = Browser.currentScene.createNode("Appearance");
    appearance.material = material;

    var shape = Browser.currentScene.createNode("Shape");
    shape.geometry = box;
    shape.appearance = appearance;
    shape.nodeid = id;

    var transform2 = Browser.currentScene.createNode("Transform");
    transform2.translation = new SFVec3f(1, 0, 0);
    var shape2 = Browser.currentScene.createNode("Shape");
    var text = Browser.currentScene.createNode("SolidText");
    shape2.geometry = text;
    shape2.appearance = appearance;
    shape2.nodeid = id;
    text.string = new MFString(name);
    transform2.children = new MFNode(shape2);
    

    var transform = Browser.currentScene.createNode("Transform");
    transform.translation = new SFVec3f(x, y, z);
    transform.rotation = new SFRotation(0,1,0,0.5);
    var children = new MFNode(shape);
    children[1] = transform2;
    transform.children = children;

    return transform;
}






