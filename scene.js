/* IMPORTS */
importScripts("settings_wikifull.js"); // graph specific settings
//importScripts("settings_wikismall.js"); // graph specific settings
//importScripts("settings_wordnet.js"); // graph specific settings

importScripts("serverconn.js"); // server connections
importScripts("nodefactory.js"); // create and recycle x3d objects
importScripts("controls.js"); // interface interaction
importScripts("search.js"); // create search results
importScripts("motion.js"); // handle motion paths
importScripts("sound.js"); // speech synthesis

/* SETTINGS */
var base_los = new SFVec3f(0,0,-1);  // base line of sight
var base_orientation = new SFRotation(0,0,1,0);  // base line of sight
var	baseurl = SETTINGS_server_url;

DEBUG = false;
if (DEBUG !== false) importScripts("debug.js");

/* GLOBALS */
var last_position;
var last_orientation;
var current_position;
var current_orientation;
var current_los; // current line of sight

var viewpoint;
//var pointer;
//

var left_node;
var right_node;
var last_select;
var last_select_name;

var force_counter;
var force_target;

mode = "explore"; // "search"

function initialize()
{
    init_serverconn(baseurl);
    init_nodefactory();
    init_controls();
    init_motion();
    init_sound();

    // tell server to reset all scene data
    server_send_reset();

    // main loop interval
    var loop_handle = setInterval(main_loop, SETTINGS_refresh_interval);
    force_target = SETTINGS_force_refresh_interval / SETTINGS_refresh_interval;

    if (DEBUG) {
        init_debug();
    }

    // get needed notes
    viewpoint = Browser.currentScene.getNamedNode("start_viewpoint");
    pointer = Browser.currentScene.getNamedNode("ts");
    pointer2 = Browser.currentScene.getNamedNode("ts2");
    pointer3 = Browser.currentScene.getNamedNode("ts3");
    pointer4 = Browser.currentScene.getNamedNode("ts4");
    search_pointer = Browser.currentScene.getNamedNode("sts");
    search_board = Browser.currentScene.getNamedNode("search_board");
    top_world = Browser.currentScene.getNamedNode("top_world");
    controls_browser = Browser.currentScene.getNamedNode("controls_browser");
    controls_close = Browser.currentScene.getNamedNode("controls_close");
    controls_board = Browser.currentScene.getNamedNode("controls_board");
    left_select_text = Browser.currentScene.getNamedNode("left_select_text");
    right_select_text = Browser.currentScene.getNamedNode("right_select_text");
    blink_sensor = Browser.currentScene.getNamedNode("blink_sensor");

    // set current position and orientation data before main loop starts
    current_position = viewpoint.position;
    current_orientation = viewpoint.orientation;

    // just a test
//    motion_startPath();
}

function main_loop() {
    current_orientation = ViewSensor.orientation_changed;
    current_position = ViewSensor.position_changed;
    current_los = current_orientation.multVec(base_los);
    
    if (current_orientation != last_orientation || current_position != last_position || force_counter > force_target) {
        // camera changed
        force_counter = 0;
        if (mode == "explore" && server_waiting === false) {
            // we're in exploration, so start updating
            last_orientation = current_orientation;
            last_position = current_position;
            cameraChanged();
        }
    } else {
        force_counter++;
    }
}

// hide the main graph
function hide_graph() {
    ultralowres.render = false;
    lowres_edges.render = false;
    lowres_nodes.render = false;
    highres_nodes.render = false;
    highres_edges.render = false;
    debug_group.render = false;
    mark_group.render = false;
}

// show the main graph
function show_graph() {
    ultralowres.render = true;
    lowres_edges.render = true;
    lowres_nodes.render = true;
    highres_nodes.render = true;
    highres_edges.render = true;
    debug_group.render = true;
    mark_group.render = true;
}

// show neighbor node group
function show_neighbors() {
    neighbors_group.render = true;
    controls_board.render = false;

}

// hide neighbor node group
function hide_neighbors() {
    neighbors_group.render = false;

}

// show search results
function show_search() {
    search_results.render = true;
    top_world.render = true;
}

// hide search results
function hide_search() {
    search_results.render = false;
    //top_world.render = false;
}

// is main graph visible?
function worldVisible() {
    return (ultralowres.render);
}


// set graph exploration mode
function setModeExplore() {
    mode = "explore";
    hide_search();
    show_neighbors();
    show_graph();
}

// set search results viewing mode
function setModeSearch() {
    mode = "search";
    hide_graph();
    hide_neighbors();
    show_search();
}

// set search results viewing mode
function setModeNeighbors() {
    mode = "neighbors";
    hide_graph();
    show_neighbors();
    hide_search();
}

function cameraChanged() {
    if (DEBUG) {
        moveAvatar(current_position);
    }
    
    server_getVisibleCells();
}

// jumps to a position for example after a node search , value is the server response
function jumpToPosition(value) {
    search_results.render = false;
    viewpoint.position = new SFVec3f(value[0], value[1], value[2]);
    viewpoint.orientation = new SFRotation(value[3], value[4], value[5], value[6]);
    cameraChanged();
}


