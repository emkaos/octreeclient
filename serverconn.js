var server_xhr;
var server_baseurl;

server_waiting = false;

function init_serverconn(baseurl){
    server_baseurl = baseurl;
    server_xhr=new XMLHttpRequest();
}

function server_send_reset(){
    var item = "reset";
    var url = baseurl + item;
    server_xhr.open('GET', url, false);
    server_xhr.send();
}

function server_updateRemote(data) {
    // to remove
    nodefactory_trashULR(data[0][2]);
    nodefactory_trashLR(data[0][1]);
    nodefactory_trashHR(data[0][0]);

    // to add
    // detail 3
    //print("To ADD "+data[1][2].length);
    var cells = data[1][2];
    if (cells.length > 0) {
        for (var i = 0; i < cells.length; i++) {
            nodefactory_queueUltraLowRes(cells[i]);
        }
    }
    // detail 2
    cells = data[1][1];
    if (cells.length > 0) {
        nodefactory_queueLowRes(cells);
    }
    
    // detail 1
    
    cells = data[1][0];
    if (cells.length > 0) {
        nodefactory_queueHighRes(cells);
    }
    
    // debug
    cells = data[1][3];
    if (cells.length > 0) {
        for (i = 0; i < cells.length; i++) {
            nodefactory_queueDebugCell(cells[i], 1, 1, 1);
        }
    }
    cells = data[0][3];
    if (cells.length > 0) {
        for (i = 0; i < cells.length; i++) {
            nodefactory_queueDebugCell(cells[i], 1, 0, 0);
        }
    }

    // mark cells
    nodes = data[1][4];
    if (nodes.length > 0) {
        for (i = 0; i < nodes.length; i++) {
            nodefactory_markNode(nodes[i]);
        }
    }

    // unmark cells
    nodes = data[0][4];
    if (nodes.length > 0) {
        for (i = 0; i < nodes.length; i++) {
            nodefactory_unmarkNode(nodes[i]);
        }
    }

    nodefactory_addAllInQueue();
}

function server_processResponse() {
    if (server_xhr.readyState == 4 && server_xhr.status == 200) {
            //var robj = json.parse(server_xhr.responsetext);
            var robj = eval('('+server_xhr.responseText+')');
            server_updateRemote(robj);
            server_waiting = false;
    }
}

function server_getVisibleCells() {
    server_waiting = true;
    var item = "pc/" + current_position.x + "/" + current_position.y + "/" + current_position.z + "/"; 
    item += current_los.x + "/" + current_los.y + "/" + current_los.z;
    var url = baseurl + item;
    server_xhr.onreadystatechange = server_processResponse;
    server_xhr.open('GET', url, true);
    server_xhr.send();
}

function server_searchNode(search_text) {
    var item = "sn/" + search_text;
    var url = baseurl + item;
    server_xhr.onreadystatechange = server_searchNodeResults;
    server_xhr.open('GET', url, true);
    server_xhr.send();
}

function server_searchNodeResults() {
    if (server_xhr.readyState == 4 && server_xhr.status == 200) {
            //var robj = json.parse(server_xhr.responsetext);
            var robj = eval('('+server_xhr.responseText+')');
            if (robj != -1) {
                search_showResults(robj);
            } else {
                setModeExplore();
            }
    }
}

function server_findNode(search_text) {
    var item = "fn/" + search_text;
    var url = baseurl + item;
    print(url);
    server_xhr.onreadystatechange = server_findNodeResults;
    server_xhr.open('GET', url, true);
    server_xhr.send();
}

function server_findNodeResults() {
    try {
        if (server_xhr.readyState == 4 && server_xhr.status == 200) {
                //var robj = json.parse(server_xhr.responsetext);
                var robj = eval('('+server_xhr.responseText+')');
                //print(robj);
                if (robj != 1) {
                if (robj != []) {
                    jumpToPosition(robj);
                }
                }
        }
    } catch(e) {
        setModeExplore();
    }
}

function server_findNodeByName(search_text) {
    var item = "fnn/" + search_text;
    var url = baseurl + item;
    print(url);
    server_xhr.onreadystatechange = server_findNodeByNameResults;
    server_xhr.open('GET', url, true);
    server_xhr.send();
}

function server_findNodeByNameResults() {
    if (server_xhr.readyState == 4 && server_xhr.status == 200) {
            //var robj = json.parse(server_xhr.responsetext);
            var robj = eval('('+server_xhr.responseText+')');
            //print(robj);
            if (robj != 1) {
            if (robj != []) {
                jumpToPosition(robj);
            }
            }
    }
}

function server_queryNode(id) {
    var item = "ni/" + id;
    var url = baseurl + item;
    server_xhr.onreadystatechange = server_query_result;
    server_xhr.open('GET', url, true);
    server_xhr.send();
}

function server_query_result() {
    if (server_xhr.readyState == 4 && server_xhr.status == 200) {
            //var robj = json.parse(server_xhr.responsetext);
            var robj = eval('('+server_xhr.responseText+')');
            if (robj != []) {
               // robj = encodeURIComponent(robj);
                last_select_name = robj;
                controls_show(SETTINGS_browser_base_url+robj);
               // controls_show("http://localhost:8070/wiki/"+robj);
            }
    }
}

function server_searchPath(left, right) {
    var item = "fp/" + left + '/' + right;
    var url = baseurl + item;
    server_xhr.onreadystatechange = server_searchPathResults;
    server_xhr.open('GET', url, true);
    server_xhr.send();
}

function server_searchPathResults() {
    if (server_xhr.readyState == 4 && server_xhr.status == 200) {
            //var robj = json.parse(server_xhr.responsetext);
            var robj = eval('('+server_xhr.responseText+')');
            if (robj != []) {
                nodefactory_createPathView(robj);
               // controls_show("http://localhost:8070/wiki/"+robj);
            }
    }
}

function server_focusNode(nodeid) {
    var item = "nn/" + nodeid;
    var url = baseurl + item;
    print("sending focus request :" + url);
    server_xhr.onreadystatechange = server_focusNodeResults;
    server_xhr.open('GET', url, true);
    server_xhr.send();
}

function server_focusNodeResults() {
    if (server_xhr.readyState == 4 && server_xhr.status == 200) {
            //var robj = json.parse(server_xhr.responsetext);
            var robj = eval('('+server_xhr.responseText+')');
            if (robj != []) {
            //print(robj);
               // controls_show("http://localhost:8070/wiki/"+robj);
               nodefactory_createNeighborView(robj);
            }
    }
}

function encode_utf8( s )
{
  return unescape(encodeURIComponent( s ) );
}

function decode_utf8( s )
{
   return decodeURIComponent( escape( s ) );
}
