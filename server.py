from loadtree import loadTree
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import json
import cgi

tree = None

class MyHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        items = self.path.split('/')
        type = int(items[-3])
        level = int(items[-2])
        number = items[-1]
        if number == "points":
            points = True
        else:
            points = False

        ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))

        content_length = int(self.headers['content-length'])
        data = self.rfile.read(content_length)
        secnums = json.loads(data)

        if points:
            rstring = self.get_sectors_by_points(type, level, secnums)
        else:
            rstring = self.get_sectors(type, level, secnums)

        self.send_response(200)
        self.send_header('Content-Length', len(rstring)) # important
        self.send_header('Content-type',    'text/json')
        self.end_headers()
        self.wfile.write(rstring)

    def do_GET(self):
        
        if self.path.endswith("debug"):
            import pdb;pdb.set_trace() 
            ret=True

        elif self.path.endswith("getTree"):
            ret = tree.getJSTree()

        else:
            items = self.path.split('/')
            type = int(items[-3])
            level = int(items[-2])
            number = int(items[-1])

            # lets try lvl 1
            # get sector
            node = tree.getNode(number, level)

            # transform to json
            if type == 2:
                ret = {}
                ret['nodes'] = node.points.items()
                ret['edges'] = node.edges

            if type == 3:
                ret = {}
                ret['coords'] = node.center 
                ret['size'] = node.size
                ret['color'] = node.getColor()
                ret['transparency'] = node.getTransparency()

                if ret['transparency'] != 1:
                    print "Color: %s, Transparency: %s" % (ret['color'], ret['transparency'])
        
        rstring = json.dumps(ret)
        rstring = "tree_Octree = " + rstring;

        self.send_response(200)
        self.send_header('Content-Length', len(rstring)) # important
        self.send_header('Content-type',    'text/json')
        self.end_headers()
        self.wfile.write(rstring)
        return

    def get_sectors(self, type, level, secnums):
        ret = "["

        if type == 3:
            for sector in secnums:
                node = tree.getNode(sector, level)
                
                if node.point_count: 
                    this = {}
                    this['transparency'] = node.getTransparency()
                    this['coords'] = node.center 
                    this['size'] = node.size
                    this['color'] = node.getColor()
                    this['sector'] = sector
                    ret += json.dumps(this) + ","

        if type == 2:
            for sector in secnums:
                node = tree.getNode(sector, level)

                if node.point_count: 
                    ret += node.json_lvl2 + ","

        if len(ret) > 1:
            ret = ret[:-1]
        ret += "]"

        return ret

    def get_sectors_by_points(self, type, level, points):
        ret = "["
        if type == 3:
            for point in points:
                node = tree.findNode(point, level)
                
                if node.point_count: 
                    this = {}
                    this['transparency'] = node.getTransparency()
                    this['coords'] = node.center 
                    this['size'] = node.size
                    this['color'] = node.getColor()
                    this['sector'] = sector
                    ret += json.dumps(this) + ","

        if type == 2:
            for point in points:
                node = tree.findNode(point, level)

                if node.getPointCount(): 
                    ret += node.json_lvl2 + ","

        if len(ret) > 1:
            ret = ret[:-1]
        ret += "]"

        return ret


def main():
    global tree
    tree = loadTree() 

    try:
        server = HTTPServer(('', 8000), MyHandler)
        print 'started httpserver...'
        server.serve_forever()
    except KeyboardInterrupt:
        print '^C received, shutting down server'
        server.socket.close()

if __name__ == '__main__':
    main()
