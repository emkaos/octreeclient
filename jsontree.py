from octree import Octree, OctreeNode
import json

class JSONTree(Octree):
    
    def createJSON(self):
        
        def rec(node):
            if node.level in [4,5,6]:
                ret = {}
                ret['nodes'] = node.getPoints()
                ret['edges'] = node.getEdges()
                node.json_lvl2 = json.dumps(ret)

            if node.is_leaf:
                pass
            else:
                for child in node.branches:
                    rec(child)

        rec(self.root)
    
