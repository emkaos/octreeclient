var debug_avatar;
var debug_viewfield;
var debug_timemap = {};
var debug_averages = {};

function init_debug() {
    debug_avatar = Browser.currentScene.getNamedNode("avatar");
    debug_viewfield = Browser.currentScene.getNamedNode("viewfield");
}

function moveAvatar(position) {
    debug_avatar.translation = position;
    debug_viewfield.translation = position;
    debug_viewfield.rotation = current_orientation;
}

function addDebugBox(position) {
    print("Adding DebugBox "+ position);

    var box = Browser.currentScene.createNode("Box");
    box.size = new SFVec3f(30, 30, 30);
    var material = Browser.currentScene.createNode("Material");
    material.diffuseColor = new SFColor(1,0,0);
    var appearance = Browser.currentScene.createNode("Appearance");
    appearance.material = material;
    var shape = Browser.currentScene.createNode("Shape");
    shape.geometry = box;
    shape.appearance = appearance;
    var transform = Browser.currentScene.createNode("Transform");
    transform.translation = position;
    transform.children = new MFNode(shape);

    debug_group.addChild(transform); 
}


function start_timer(key) {
    debug_timemap[key] = (new Date).getTime();
}

function stop_timer(key) {
    if (debug_timemap[key]) {
        var d = (new Date).getTime() - debug_timemap[key];
        if (!debug_averages[key]) {
            debug_averages[key] = [1,d];
        } else {
            var l = debug_averages[key];
            l[1] = (l[0] * l[1] + d) / (l[0] + 1);
            l[0]++;
            debug_averages[key] = l;
        }
    }
}

function print_results(key) {
    if (debug_averages[key]) {
        var l = debug_averages[key];
        print(l[0] + " runs for " + key + " with average time " + l[1] + "ms" );
    } else {
        print("Key not found!");
        }
        }
